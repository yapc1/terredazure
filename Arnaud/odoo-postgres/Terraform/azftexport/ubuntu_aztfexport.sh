#!/bin/bash
echo "Utilisez ce script en tant que root"
 
# Mise à jour
apt update
apt upgrade -y
 
# Installation de codium
snap install codium --classic
 
#Installation d'azure cli
apt install -y curl
curl -sL https://aka.ms/InstallAzureCLIDeb | bash
 
#Installation de Terraform
apt-get update && apt-get install -y gnupg software-properties-common
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | tee /usr/share/keyrings/hashicorp-archive-keyring.gpg > /dev/null
gpg --no-default-keyring --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg --fingerprint
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list
apt update
apt install -y terraform
 
#Installation d'Aztfexport
curl -sSL https://packages.microsoft.com/keys/microsoft.asc > /etc/apt/trusted.gpg.d/microsoft.asc
apt-add-repository -y https://packages.microsoft.com/ubuntu/22.04/prod
apt install -y aztfexport

echo " en root : "
echo "Utilisation :créez un dossier de travail"
echo "Utilisation :az login --use-device-code"
echo "Utilisation :récupération de l'infra"
echo "Utilisation :aztfexport resource-group myResourceGroup"
echo " w pour importer les ressources"