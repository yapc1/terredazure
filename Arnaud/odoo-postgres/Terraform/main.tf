resource "azurerm_resource_group" "res-0" {
  location = "francecentral"
  name     = "OCC_ASD_Equipe4"
}
resource "azurerm_ssh_public_key" "res-1" {
  location            = "francecentral"
  name                = "odoo_ssh_pub_key"
  public_key          = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDI/LwxoZazNHrJucPEyJb9IPwai6KWwQ3wp9ACUmYxG3RT7IK/pccK5XUQTdrtflMlgVAilrb3np6+I/tmXELyShQYNOe6eI6tqCfAYnCd3YCNeQgUt56b6629sibwLN/fqQzZmdCwrOrgLKVJ0vNb01JHJJrYQGkWF/gs5ST7fPhqKPYB7+423w24VYKJOWX4sjy/u9CRIQR4Peb+8ZJV9H5DevUAkCziIxlckmEwKfAPxhu4cTWbElCcH7EjmmkKA9MC1oOTSgSSqoYxfpsPZUJNvnMMZm4srErfmcsel5hF+7PI8aWSuR+8R8wndV5hk92dP9N8gEN6eylOmV84B2NxwrTeUc5aRMO/aa8XVvRA31FJFBhGY0WCtDRSfkrJsquZY9EHBUcAtPlPn9qzxr/qVJBhdu4XsHnhyA4XxyDoLgzf1KiIS/IO8UzFPbrFal2RNzGCf/1g88BuxkYRZa2ke+GLd7zj/23big37J1UksQ30/H6I1Y/ovzD5jsU= generated-by-azure"
  resource_group_name = "OCC_ASD_Equipe4"
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_linux_virtual_machine_scale_set" "res-2" {
  admin_username         = "adminodoo"
  instances              = 1
  location               = "francecentral"
  name                   = "VmssOdoo"
  overprovision          = false
  resource_group_name    = "OCC_ASD_Equipe4"
  secure_boot_enabled    = true
  single_placement_group = false
  sku                    = "Standard_B1s"
  upgrade_mode           = "Automatic"
  vtpm_enabled           = true
  zones                  = ["1", "2", "3"]
  admin_ssh_key {
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDI/LwxoZazNHrJucPEyJb9IPwai6KWwQ3wp9ACUmYxG3RT7IK/pccK5XUQTdrtflMlgVAilrb3np6+I/tmXELyShQYNOe6eI6tqCfAYnCd3YCNeQgUt56b6629sibwLN/fqQzZmdCwrOrgLKVJ0vNb01JHJJrYQGkWF/gs5ST7fPhqKPYB7+423w24VYKJOWX4sjy/u9CRIQR4Peb+8ZJV9H5DevUAkCziIxlckmEwKfAPxhu4cTWbElCcH7EjmmkKA9MC1oOTSgSSqoYxfpsPZUJNvnMMZm4srErfmcsel5hF+7PI8aWSuR+8R8wndV5hk92dP9N8gEN6eylOmV84B2NxwrTeUc5aRMO/aa8XVvRA31FJFBhGY0WCtDRSfkrJsquZY9EHBUcAtPlPn9qzxr/qVJBhdu4XsHnhyA4XxyDoLgzf1KiIS/IO8UzFPbrFal2RNzGCf/1g88BuxkYRZa2ke+GLd7zj/23big37J1UksQ30/H6I1Y/ovzD5jsU= generated-by-azure"
    username   = "adminodoo"
  }
  boot_diagnostics {
  }
  identity {
    type = "SystemAssigned"
  }
  network_interface {
    name                      = "NicVmssOdoo"
    network_security_group_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Network/networkSecurityGroups/VmssOdoo-nsg"
    primary                   = true
    ip_configuration {
      application_gateway_backend_address_pool_ids = ["/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Network/applicationGateways/AppGatewayTA/backendAddressPools/BackendOdoo"]
      name                                         = "NicVmssOdoo-defaultIpConfiguration"
      primary                                      = true
      subnet_id                                    = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Network/virtualNetworks/Vnet_TA/subnets/Backend_subnet_TA"
    }
  }
  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "StandardSSD_LRS"
  }
  source_image_reference {
    offer     = "0001-com-ubuntu-server-jammy"
    publisher = "canonical"
    sku       = "22_04-lts-gen2"
    version   = "latest"
  }
  depends_on = [
    azurerm_application_gateway.res-6,
    azurerm_network_security_group.res-8,
    azurerm_subnet.res-20,
  ]
}
resource "azurerm_virtual_machine_scale_set_extension" "res-3" {
  name                         = "AzureMonitorLinuxAgent"
  publisher                    = "Microsoft.Azure.Monitor"
  type                         = "AzureMonitorLinuxAgent"
  type_handler_version         = "1.0"
  virtual_machine_scale_set_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Compute/virtualMachineScaleSets/VmssOdoo"
  depends_on = [
    azurerm_linux_virtual_machine_scale_set.res-2,
  ]
}
resource "azurerm_virtual_machine_scale_set_extension" "res-4" {
  name      = "CustomScript"
  publisher = "Microsoft.Azure.Extensions"
  settings = jsonencode({
    commandToExecute = "./custom_app_odoo.sh"
    fileUris         = ["https://gitlab.com/yapc1/terredazure/-/raw/main/Arnaud/odoo-postgres/customapp/custom_app_odoo.sh"]
  })
  type                         = "CustomScript"
  type_handler_version         = "2.0"
  virtual_machine_scale_set_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Compute/virtualMachineScaleSets/VmssOdoo"
  depends_on = [
    azurerm_linux_virtual_machine_scale_set.res-2,
  ]
}
resource "azurerm_postgresql_flexible_server" "res-5" {
  location            = "francecentral"
  name                = "db-psql-odoo-ta"
  resource_group_name = "OCC_ASD_Equipe4"
  zone                = "1"
  sku_name            = "GP_Standard_D2ds_v5"
  version             = "16"
  administrator_login         = "adminodoo"
  administrator_password      = "Azerty123"
  storage_mb             = 32768
  backup_retention_days  = 7
  high_availability {
    mode                      = "SameZone"
    standby_availability_zone = "1"
  }
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_application_gateway" "res-6" {
  enable_http2        = true
  location            = "francecentral"
  name                = "AppGatewayTA"
  resource_group_name = "OCC_ASD_Equipe4"
  zones               = ["1", "2", "3"]
  autoscale_configuration {
    max_capacity = 10
    min_capacity = 1
  }
  backend_address_pool {
    name = "BackendOdoo"
  }
  backend_http_settings {
    affinity_cookie_name  = "ApplicationGatewayAffinity"
    cookie_based_affinity = "Enabled"
    name                  = "Http"
    port                  = 80
    protocol              = "Http"
    request_timeout       = 20
  }
  frontend_ip_configuration {
    name                 = "appGwPublicFrontendIpIPv4"
    public_ip_address_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Network/publicIPAddresses/AppGateway_pub_Ip_TA"
  }
  frontend_port {
    name = "port_80"
    port = 80
  }
  gateway_ip_configuration {
    name      = "appGatewayIpConfig"
    subnet_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Network/virtualNetworks/Vnet_TA/subnets/AppGateway_TA_subnet"
  }
  http_listener {
    frontend_ip_configuration_name = "appGwPublicFrontendIpIPv4"
    frontend_port_name             = "port_80"
    name                           = "HttpListener"
    protocol                       = "Http"
  }
  request_routing_rule {
    backend_address_pool_name  = "BackendOdoo"
    backend_http_settings_name = "Http"
    http_listener_name         = "HttpListener"
    name                       = "HttpRule"
    priority                   = 100
    rule_type                  = "Basic"
  }
  sku {
    name = "WAF_v2"
    tier = "WAF_v2"
  }
  waf_configuration {
    enabled          = true
    firewall_mode    = "Prevention"
    rule_set_version = "3.0"
  }
  depends_on = [
    azurerm_public_ip.res-14,
    azurerm_subnet.res-19,
  ]
}
resource "azurerm_firewall" "res-7" {
  location            = "francecentral"
  name                = "FirewallTA"
  resource_group_name = "OCC_ASD_Equipe4"
  sku_name            = "AZFW_VNet"
  sku_tier            = "Standard"
  zones               = ["1", "2", "3"]
  ip_configuration {
    name                 = "Firewall_pub_Ip_TA"
    public_ip_address_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Network/publicIPAddresses/Firewall_pub_Ip_TA"
    subnet_id            = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Network/virtualNetworks/Vnet_TA/subnets/AzureFirewallSubnet"
  }
  depends_on = [
    azurerm_public_ip.res-15,
    azurerm_subnet.res-18,
  ]
}
resource "azurerm_network_security_group" "res-8" {
  location            = "francecentral"
  name                = "VmssOdoo-nsg"
  resource_group_name = "OCC_ASD_Equipe4"
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_network_security_rule" "res-9" {
  access                      = "Allow"
  destination_address_prefix  = "*"
  destination_port_range      = "80"
  direction                   = "Inbound"
  name                        = "AllowAnyHTTPInbound"
  network_security_group_name = "VmssOdoo-nsg"
  priority                    = 1010
  protocol                    = "Tcp"
  resource_group_name         = "OCC_ASD_Equipe4"
  source_address_prefix       = "*"
  source_port_range           = "*"
  depends_on = [
    azurerm_network_security_group.res-8,
  ]
}
resource "azurerm_network_security_rule" "res-10" {
  access                      = "Allow"
  destination_address_prefix  = "*"
  destination_port_range      = "80"
  direction                   = "Outbound"
  name                        = "AllowAnyHTTPOutbound"
  network_security_group_name = "VmssOdoo-nsg"
  priority                    = 1020
  protocol                    = "Tcp"
  resource_group_name         = "OCC_ASD_Equipe4"
  source_address_prefix       = "*"
  source_port_range           = "*"
  depends_on = [
    azurerm_network_security_group.res-8,
  ]
}
resource "azurerm_network_security_rule" "res-11" {
  access                      = "Allow"
  destination_address_prefix  = "*"
  destination_port_range      = "22"
  direction                   = "Inbound"
  name                        = "default-allow-ssh"
  network_security_group_name = "VmssOdoo-nsg"
  priority                    = 1000
  protocol                    = "Tcp"
  resource_group_name         = "OCC_ASD_Equipe4"
  source_address_prefix       = "*"
  source_port_range           = "*"
  depends_on = [
    azurerm_network_security_group.res-8,
  ]
}
resource "azurerm_private_dns_zone" "res-12" {
  name                = "privatelink.postgres.database.azure.com"
  resource_group_name = "OCC_ASD_Equipe4"
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_private_dns_zone_virtual_network_link" "res-12b" {
  name                  = "d4vp67vhjyixm"
  private_dns_zone_name = "privatelink.postgres.database.azure.com"
  resource_group_name   = "OCC_ASD_Equipe4"
  virtual_network_id    = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Network/virtualNetworks/Vnet_TA"
  depends_on = [
    azurerm_private_dns_zone.res-12,
    azurerm_virtual_network.res-16,
  ]
}
resource "azurerm_private_endpoint" "res-13" {
  location            = "francecentral"
  name                = "EndpointDbTA"
  resource_group_name = "OCC_ASD_Equipe4"
  subnet_id           = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Network/virtualNetworks/Vnet_TA/subnets/Backend_subnet_TA"
  private_dns_zone_group {
    name                 = "default"
    private_dns_zone_ids = ["/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Network/privateDnsZones/privatelink.postgres.database.azure.com"]
  }
  private_service_connection {
    is_manual_connection           = false
    name                           = "EndpointDbTA_b3e417c4-a55e-49ac-954a-79b3e69a101c"
    private_connection_resource_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-psql-odoo-ta"
    subresource_names              = ["postgresqlServer"]
  }
  depends_on = [
    azurerm_postgresql_flexible_server.res-5,
    azurerm_private_dns_zone.res-12,
    azurerm_subnet.res-20,
  ]
}
resource "azurerm_public_ip" "res-14" {
  allocation_method   = "Static"
  location            = "francecentral"
  name                = "AppGateway_pub_Ip_TA"
  resource_group_name = "OCC_ASD_Equipe4"
  sku                 = "Standard"
  zones               = ["1", "2", "3"]
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_public_ip" "res-15" {
  allocation_method   = "Static"
  location            = "francecentral"
  name                = "Firewall_pub_Ip_TA"
  resource_group_name = "OCC_ASD_Equipe4"
  sku                 = "Standard"
  zones               = ["1", "2", "3"]
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_virtual_network" "res-16" {
  address_space       = ["10.1.0.0/16"]
  location            = "francecentral"
  name                = "Vnet_TA"
  resource_group_name = "OCC_ASD_Equipe4"
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_subnet" "res-17" {
  address_prefixes     = ["10.1.3.0/26"]
  name                 = "AzureBastionSubnet"
  resource_group_name  = "OCC_ASD_Equipe4"
  virtual_network_name = "Vnet_TA"
  depends_on = [
    azurerm_virtual_network.res-16,
  ]
}
resource "azurerm_subnet" "res-18" {
  address_prefixes     = ["10.1.0.0/26"]
  name                 = "AzureFirewallSubnet"
  resource_group_name  = "OCC_ASD_Equipe4"
  virtual_network_name = "Vnet_TA"
  depends_on = [
    azurerm_virtual_network.res-16,
  ]
}
resource "azurerm_subnet" "res-19" {
  address_prefixes     = ["10.1.1.0/24"]
  name                 = "AppGateway_TA_subnet"
  resource_group_name  = "OCC_ASD_Equipe4"
  virtual_network_name = "Vnet_TA"
  depends_on = [
    azurerm_virtual_network.res-16,
  ]
}
resource "azurerm_subnet" "res-20" {
  address_prefixes     = ["10.1.2.0/24"]
  name                 = "Backend_subnet_TA"
  resource_group_name  = "OCC_ASD_Equipe4"
  virtual_network_name = "Vnet_TA"
  depends_on = [
    azurerm_virtual_network.res-16,
  ]
}
resource "azurerm_monitor_action_group" "res-21" {
  name                = "cpu alert"
  resource_group_name = "OCC_ASD_Equipe4"
  short_name          = "cpu alert"
  email_receiver {
    email_address           = "nonofx31@gmail.com"
    name                    = "Email0_-EmailAction-"
    use_common_alert_schema = true
  }
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_monitor_autoscale_setting" "res-22" {
  location            = "francecentral"
  name                = "VmssOdooautoscale"
  resource_group_name = "OCC_ASD_Equipe4"
  target_resource_id  = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Compute/virtualMachineScaleSets/VmssOdoo"
  profile {
    name = "ScaleRuleOdoo"
    capacity {
      default = 1
      maximum = 8
      minimum = 1
    }
    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_namespace   = "microsoft.compute/virtualmachinescalesets"
        metric_resource_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Compute/virtualMachineScaleSets/VmssOdoo"
        operator           = "GreaterThan"
        statistic          = "Average"
        threshold          = 50
        time_aggregation   = "Average"
        time_grain         = "PT1M"
        time_window        = "PT2M"
      }
      scale_action {
        cooldown  = "PT5M"
        direction = "Increase"
        type      = "ChangeCount"
        value     = 1
      }
    }
    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_namespace   = "microsoft.compute/virtualmachinescalesets"
        metric_resource_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Compute/virtualMachineScaleSets/VmssOdoo"
        operator           = "LessThan"
        statistic          = "Average"
        threshold          = 20
        time_aggregation   = "Average"
        time_grain         = "PT1M"
        time_window        = "PT2M"
      }
      scale_action {
        cooldown  = "PT5M"
        direction = "Decrease"
        type      = "ChangeCount"
        value     = 1
      }
    }
  }
  depends_on = [
    azurerm_linux_virtual_machine_scale_set.res-2,
  ]
}
resource "azurerm_monitor_metric_alert" "res-23" {
  name                = "cpu alert"
  resource_group_name = "OCC_ASD_Equipe4"
  scopes              = ["/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Compute/virtualMachineScaleSets/VmssOdoo"]
  severity            = 2
  action {
    action_group_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/microsoft.insights/actiongroups/cpu alert"
  }
  criteria {
    aggregation      = "Average"
    metric_name      = "CPU Credits Remaining"
    metric_namespace = "Microsoft.Compute/virtualMachineScaleSets"
    operator         = "GreaterThan"
    threshold        = 50
  }
  depends_on = [
    azurerm_linux_virtual_machine_scale_set.res-2,
  ]
}
resource "azurerm_private_dns_a_record" "res-24" {
  name                = "db-psql-odoo-ta"
  records             = ["10.1.2.5"]
  resource_group_name = "OCC_ASD_Equipe4"
  tags = {
    creator = "created by private endpoint EndpointDbTA with resource guid 1fb90e0f-b5ae-4bda-8d97-65524fe66e2a"
  }
  ttl       = 10
  zone_name = "privatelink.postgres.database.azure.com"
  depends_on = [
    azurerm_private_dns_zone.res-12,
  ]
}
