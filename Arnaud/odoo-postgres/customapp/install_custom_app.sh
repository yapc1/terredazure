myResourceGroup=OCC_ASD_Arnaud
ScaleSet=VmssOdoo

az vmss extension set \
    --publisher Microsoft.Azure.Extensions \
    --version 2.0 \
    --name CustomScript \
    --resource-group $myResourceGroup \
    --vmss-name $ScaleSet \
    --settings @configodoo.json