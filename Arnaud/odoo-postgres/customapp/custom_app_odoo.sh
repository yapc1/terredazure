#!/bin/bash

# Mise à jour du gestionnaire de paquets
sudo apt update
sudo apt upgrade -y

# Installation de Docker
sudo apt install -y docker.io

# Installation de Docker-compose
sudo apt install -y docker-compose

# Installation du client PostgreSQL
sudo apt install -y postgresql-client

# Ajout de l'utilisateur adminodoo au groupe docker
sudo usermod -aG docker adminodoo

# Téléchargement de docker-compose.yml
sudo curl -o /home/adminodoo/docker-compose.yml https://gitlab.com/yapc1/terredazure/-/raw/main/Arnaud/odoo-postgres/docker-compose.yml

# Exécution de docker-compose up
sudo docker-compose -f /home/adminodoo/docker-compose.yml up -d

# Téléchargement de odoo.conf
# sudo curl -o /home/adminodoo/conf/odoo.conf https://gitlab.com/yapc1/terredazure/-/raw/main/Arnaud/odoo-postgres/odoo.conf