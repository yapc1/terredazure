# Liste des ressources avec liens

- [Azure Terraform](https://learn.microsoft.com/fr-fr/azure/developer/terraform/overview)
    - [GitLab dédié](https://gitlab.com/arnaudmarty/terraform)

- [Azure Ansible](https://learn.microsoft.com/fr-fr/azure/developer/ansible/overview)

- [Azure Application Gateway](https://learn.microsoft.com/en-us/azure/application-gateway/overview?source=recommendations)
    - avec Azure Web Application Firewall (WAF)

- [Azure Virtual Machine Scale Sets](https://learn.microsoft.com/en-us/azure/virtual-machine-scale-sets/overview)

- [Docker Odoo](https://hub.docker.com/_/odoo/) : Image Azure Ubuntu Docker custom
    - `imageID="ntegralinc1586961136942:ntg_ubuntu_22_04_docker:ntg_ubuntu_22_04_docker:1.0.0"` (Obtenir l'Urn dans le CLI Azure avec --az vm image list--)
    - `sizeVM="Standard_B2s"` (évolutif)
    - `storageSKU="StandardSSD_LRS"`

- [Serveur flexible](https://learn.microsoft.com/fr-fr/azure/postgresql/flexible-server/overview?source=recommendations) - Azure Database pour PostgreSQL (niveaux Usage général ou À mémoire optimisée)
    - haute disponibilité
    - mise à l’échelle
    - module de chiffrement conforme à la norme FIPS 140-2
    - monitoring des performances et alerte

- [Backup and restore](https://learn.microsoft.com/en-us/azure/postgresql/flexible-server/concepts-backup-restore) in Azure Database for PostgreSQL - Flexible Server

- [Stockage Blob Azure](https://learn.microsoft.com/fr-fr/azure/storage/blobs/storage-blobs-introduction)

- [Azure Key Vault](https://learn.microsoft.com/fr-fr/azure/key-vault/general/basic-concepts)
    - [GitLab dédié](https://gitlab.com/arnaudmarty/azure-vault)

- [Azure Firewall](https://learn.microsoft.com/fr-fr/azure/firewall/overview)

- [Azure DDos Protection](https://learn.microsoft.com/fr-fr/azure/ddos-protection/ddos-protection-overview)

- [Azure Monitor](https://learn.microsoft.com/fr-fr/azure/azure-monitor/overview)

- [Nom de Domain NoIp.com](https://www.noip.com)

# integration postegres/VmSS ubuntu

- VM : 
    - `cd /home/adminodoo`
    - `apt update && apt upgrade`
    - `apt install postgresql-client`
    - `psql --host=db-odoo-ta.postgres.database.azure.com --port=5432 --username=adminodoo --dbname=postgres`
- test connection dbpg/vm
![alt text](<pictures/01 test connection dbpg vm.png>)

> On verifie aussi que la tunnelisation entre la VM et la DB est bien en TLS v1.3

## contenaire odoo connecté à azure db postgreSQL :
- [docker-compose.yml](https://gitlab.com/yapc1/terredazure/-/blob/main/Arnaud/odoo%20postgres/docker-compose.yml?ref_type=heads)

  - `wget https://gitlab.com/yapc1/terredazure/-/raw/main/Arnaud/odoo-postgres/docker-compose.yml`

```yml
version: '3.3'
services:
    odoo:
        container_name: odoo15
        image: odoo:15.0
        restart: always
        tty: true
        networks:
            - default
        ports:
            - "80:8069"
        volumes:
            - odoo-data:/var/lib/odoo:delegated
            - ~/odoo15e/enterprise:/mnt/enterprise:delegated
            - ~/odoo15e/design-themes:/mnt/design-themes:delegated
            - ./conf:/etc/odoo
        environment:
            - DB_PORT_5432_TCP_ADDR=db-psql-odoo-test.postgres.database.azure.com
            - DB_PORT_5432_TCP_PORT=5432
            - DB_ENV_POSTGRES_USER=adminodoo
            - DB_ENV_POSTGRES_PASSWORD=Azerty123
networks:
     default:
         driver: bridge
volumes:
    odoo-data:
```

- [odoo.conf](https://gitlab.com/yapc1/terredazure/-/blob/main/Arnaud/odoo%20postgres/odoo.conf?ref_type=heads)

    - `wget https://gitlab.com/yapc1/terredazure/-/raw/main/Arnaud/odoo-postgres/odoo.conf`
    - `sudo mv odoo.conf ./conf/odoo.conf`
- test connection odoo
    - http:// IP-App-Gateway
    ![alt text](<pictures/02 test connection odoo.png>)

# Provisionnement avec Ansible

Bien que nous ne pouvons pas utiliser Ansible pour le provisionnement des instances dans le VmScaleSet, nous allons tout de même en faire la demonstration pour respecter le cadre du TP.

### recherche solution de connection ssh VMSS pour ansible.
    - ip public : pas une solution, il crée un ip par instance et on ne peut pas les enlever après
    - tunnel bastion : incompatible avec le cloud shell
    - firewall : ok

- regle nat firewall : 

![alt text](pictures/parefeu-regle-nat.png)

- connection ssh cloud-shell/vmssOdoo par l'ip public du firewall

![alt text](pictures/ssh-vmss-parefeu.png)

### Ansible : Déploiement docker, docker-compose, postgreSQL-client, odoo

  - Probleme d'affichage image et icone suite au deploiement avec ansible : 

![alt text](pictures/pblm_icone.png)

>Après une longue recherche sur la cause, j'ai identifié un probleme de permission. Le provisionnement ansible ce fait en root à la racine `/`

>L'utilisateur adminiodoo n'a donc pas toute les permitions requises au bon fonctionnement de odoo. J'ai modifié mon playbook ansible pour y remédier.

#### [playbookodoo.yml](https://gitlab.com/yapc1/terredazure/-/blob/main/Arnaud/odoo-postgres/ansible/playbookodoo.yml?ref_type=heads)

```yml
---
- name: Déploiement de Docker, Docker-compose et Odoo avec Ansible
  hosts: servers_odoo
  become: yes
  remote_user: adminodoo
  tasks:
    - name: Mise à jour du gestionnaire de paquets
      apt:
        update_cache: yes
        upgrade: yes

    - name: Installation de Docker
      apt:
        name: docker.io
        state: present

    - name: Installation de Docker-compose
      apt:
        name: docker-compose
        state: present

    - name: Installation du client PostgreSQL
      apt:
        name: postgresql-client
        state: present

    - name: Ajout de l'utilisateur adminodoo au groupe docker
      user:
        name: adminodoo
        groups: docker
        append: yes

    - name: Création du répertoire /conf
      file:
        path: "/home/adminodoo/conf"
        state: directory

    - name: Téléchargement de docker-compose.yml
      get_url:
        url: "https://gitlab.com/yapc1/terredazure/-/raw/main/Arnaud/odoo-postgres/docker-compose.yml"
        dest: "/home/adminodoo/docker-compose.yml"

    - name: Exécuter docker-compose up
      command: docker-compose -f /home/adminodoo/docker-compose.yml up -d

    - name: Téléchargement de odoo.conf
      get_url:
        url: "https://gitlab.com/yapc1/terredazure/-/raw/main/Arnaud/odoo-postgres/odoo.conf"
        dest: "/home/adminodoo/conf/odoo.conf"

  vars:
    ansible_ssh_private_key_file: /home/arnaud/.ssh/id_rsa_TA
```

# VMSS

### Scale Set

Pour le vmss, nous avons mis en place cette politique de scale :
- scale out à 70% de l'uc avec un intervalle de temps de 10 minutes
- scale in à 30% de l'uc avec un intervalle de temps de 10 minutes

La vm0 est protégé pour le scale in

### Provision des instances

Pour le provisionnement des instances lors d'un scale out, nous créons un script d'[extention custom](https://learn.microsoft.com/en-us/azure/virtual-machines/extensions/custom-script-linux) dans le module "Applications + Extensions"

![alt text](pictures/05_provision_scaleset.png)

Pour créer cette extention, nous utilisons ces script dans azure CLI

#### *[install_custom_app.sh](https://gitlab.com/yapc1/terredazure/-/blob/main/Arnaud/odoo-postgres/customapp/install_custom_app.sh?ref_type=heads)*

```sh
myResourceGroup=OCC_ASD_CapitaineAsdOcc_7
ScaleSet=VmssOdoo

az vmss extension set \
    --publisher Microsoft.Azure.Extensions \
    --version 2.0 \
    --name CustomScript \
    --resource-group $myResourceGroup \
    --vmss-name $ScaleSet \
    --settings @configodoo.json
```

#### *[configodoo.json](https://gitlab.com/yapc1/terredazure/-/blob/main/Arnaud/odoo-postgres/customapp/configodoo.json?ref_type=heads)*

```json
{
  "fileUris": ["https://gitlab.com/yapc1/terredazure/-/raw/main/Arnaud/odoo-postgres/customapp/custom_app_odoo.sh"],
  "commandToExecute": "./custom_app_odoo.sh"
}
```

#### *[custom_app_odoo.sh](https://gitlab.com/yapc1/terredazure/-/blob/main/Arnaud/odoo-postgres/customapp/custom_app_odoo.sh?ref_type=heads)*

```sh
#!/bin/bash

# Mise à jour du gestionnaire de paquets
sudo apt update
sudo apt upgrade -y

# Installation de Docker
sudo apt install -y docker.io

# Installation de Docker-compose
sudo apt install -y docker-compose

# Installation du client PostgreSQL
sudo apt install -y postgresql-client

# Ajout de l'utilisateur adminodoo au groupe docker
sudo usermod -aG docker adminodoo

# Téléchargement de docker-compose.yml
sudo curl -o /home/adminodoo/docker-compose.yml https://gitlab.com/yapc1/terredazure/-/raw/main/Arnaud/odoo-postgres/docker-compose.yml

# Exécution de docker-compose up
sudo docker-compose -f /home/adminodoo/docker-compose.yml up -d

# Téléchargement de odoo.conf
sudo curl -o /home/adminodoo/conf/odoo.conf https://gitlab.com/yapc1/terredazure/-/raw/main/Arnaud/odoo-postgres/odoo.conf
```

![alt text](pictures/05_provision_scaleset_extention.png)

### Application Gateway

Pour que Odoo fonctionne sur plusieurs instances, nous devons paramétrer le back-end de l'AppGateway avec `Affinité basée sur les cookies` **activé**

![alt text](pictures/06_Vmss_AppGat01.png)

Sinon le token CSRF ([Cross-site request forgery](https://www.vaadata.com/blog/fr/attaques-csrf-principes-impacts-exploitations-bonnes-pratiques-securite/)) généré par odoo sera invalid dés qu'il y aura plus d'une intance Vmss.

![alt text](pictures/06_Vmss_AppGat02.png)



# [Backup d'urgence postgresql](https://learn.microsoft.com/fr-fr/azure/postgresql/flexible-server/concepts-backup-restore#long-term-retention-preview)

- dans le flexiserver azure database posgresql
    - dans sauvegarde et restauration
        - parametre de la durée de conservation des sauvegardes journalières d'urgence
        - restauration rapide de la sauvegarde choisie

![alt text](pictures/04_backup_urgence_db1.png)

- on va alors déployer un nouveau serveur avec la db restauré à partir du point de sauvegarde choisi

![alt text](pictures/04_backup_urgence_db2.png)

- il faudrat alors remplacer l'ancien serveur par le nouveau en remplaçant l'ancien endpoint par le nouveau créé avec le serveur restauré.

# [Backup d'archive postgresql](https://learn.microsoft.com/fr-fr/azure/backup/backup-azure-database-postgresql-flex?source=recommendations)

- deploiement d'un backup vault
- definition d'une stratégie de sauvegarde

![alt text](pictures/03_backup_db1.png)

![alt text](pictures/03_backup_db2.png)

![alt text](pictures/03_backup_db3.png)

![alt text](pictures/03_backup_db4.png)

- on va pouvoir paramettrer une sauvegarde hebdonadaire ainsi que la durée de retention
    - pour le coffre d'archive, la durée peut aller jusqu'à 10 ans

![alt text](pictures/03_backup_db5.png)

- on peut lancer une sauvegarde non planifié en passant par `Instance de sauvegarde`

![alt text](pictures/03_backup_db6.png)

### Politique de backup

- une backup d'urgence par jour avec 7 jours de rétention
- une backup dans le vault chaque semaine (vendredi 23h00) avec 3 mois de retention
- une backup d'archive dans le vault lancé manuellement une fois par an (fin d'excercice fiscal) avec une retention de 10 ans

# [aztfexport](https://learn.microsoft.com/fr-fr/azure/developer/terraform/azure-export-for-terraform/export-terraform-concepts?source=recommendations)

### Installation aztfexport

Lien [Github aztfexport](https://github.com/azure/aztfexport)

Nous avons choisie de l'installer sur une VM Ubuntu 22.04 avec Virtual box

- Nous installons aztfexport et ces dépendances avec ce script : 
  - `wget https://gitlab.com/yapc1/terredazure/-/raw/main/Arnaud/odoo-postgres/Terraform/azftexport/ubuntu_aztfexport.sh`

**[ubuntu_aztfexport.sh](https://gitlab.com/yapc1/terredazure/-/tree/main/Arnaud/odoo-postgres/Terraform/azftexport?ref_type=heads)**

```sh
#!/bin/bash
echo "Utilisez ce script en tant que root"
 
# Mise à jour
apt update
apt upgrade -y
 
# Installation de codium
snap install codium --classic
 
#Installation d'azure cli
apt install -y curl
curl -sL https://aka.ms/InstallAzureCLIDeb | bash
 
#Installation de Terraform
apt-get update && apt-get install -y gnupg software-properties-common
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | tee /usr/share/keyrings/hashicorp-archive-keyring.gpg > /dev/null
gpg --no-default-keyring --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg --fingerprint
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list
apt update
apt install -y terraform
 
#Installation d'Aztfexport
curl -sSL https://packages.microsoft.com/keys/microsoft.asc > /etc/apt/trusted.gpg.d/microsoft.asc
apt-add-repository -y https://packages.microsoft.com/ubuntu/22.04/prod
apt install -y aztfexport

echo " en root : "
echo "Utilisation :créez un dossier de travail"
echo "Utilisation :az login --use-device-code"
echo "Utilisation :récupération de l'infra"
echo "Utilisation :aztfexport resource-group myResourceGroup"
echo " w pour importer les ressources"
```

- Connection Azure Login
  - `az login --use-device-code`

![alt text](pictures/07_azftexport_az_login.png)

- `aztfexport resource-group myResourceGroup`
  - `w` pour importer les ressources

![alt text](pictures/07_azftexport_extract_running.png)

!![alt text](pictures/07_azftexport_extract_end.png)

# deploiement Terraform

**[provider.tf](https://gitlab.com/yapc1/terredazure/-/blob/main/Arnaud/odoo-postgres/Terraform/provider.tf?ref_type=heads)**

```tf
provider "azurerm" {
  features {
  }
}
```

**[terraform.tf](https://gitlab.com/yapc1/terredazure/-/blob/main/Arnaud/odoo-postgres/Terraform/terraform.tf?ref_type=heads)**

```tf
terraform {
  backend "local" {}

  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "3.99.0"

    }
  }
}
```

**[main.tf](https://gitlab.com/yapc1/terredazure/-/blob/main/Arnaud/odoo-postgres/Terraform/main.tf?ref_type=heads)**

```tf
resource "azurerm_resource_group" "res-0" {
  location = "francecentral"
  name     = "OCC_ASD_Equipe4"
}
resource "azurerm_ssh_public_key" "res-1" {
  location            = "francecentral"
  name                = "odoo_ssh_pub_key"
  public_key          = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDI/LwxoZazNHrJucPEyJb9IPwai6KWwQ3wp9ACUmYxG3RT7IK/pccK5XUQTdrtflMlgVAilrb3np6+I/tmXELyShQYNOe6eI6tqCfAYnCd3YCNeQgUt56b6629sibwLN/fqQzZmdCwrOrgLKVJ0vNb01JHJJrYQGkWF/gs5ST7fPhqKPYB7+423w24VYKJOWX4sjy/u9CRIQR4Peb+8ZJV9H5DevUAkCziIxlckmEwKfAPxhu4cTWbElCcH7EjmmkKA9MC1oOTSgSSqoYxfpsPZUJNvnMMZm4srErfmcsel5hF+7PI8aWSuR+8R8wndV5hk92dP9N8gEN6eylOmV84B2NxwrTeUc5aRMO/aa8XVvRA31FJFBhGY0WCtDRSfkrJsquZY9EHBUcAtPlPn9qzxr/qVJBhdu4XsHnhyA4XxyDoLgzf1KiIS/IO8UzFPbrFal2RNzGCf/1g88BuxkYRZa2ke+GLd7zj/23big37J1UksQ30/H6I1Y/ovzD5jsU= generated-by-azure"
  resource_group_name = "OCC_ASD_Equipe4"
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_linux_virtual_machine_scale_set" "res-2" {
  admin_username         = "adminodoo"
  instances              = 1
  location               = "francecentral"
  name                   = "VmssOdoo"
  overprovision          = false
  resource_group_name    = "OCC_ASD_Equipe4"
  secure_boot_enabled    = true
  single_placement_group = false
  sku                    = "Standard_B1s"
  upgrade_mode           = "Automatic"
  vtpm_enabled           = true
  zones                  = ["1", "2", "3"]
  admin_ssh_key {
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDI/LwxoZazNHrJucPEyJb9IPwai6KWwQ3wp9ACUmYxG3RT7IK/pccK5XUQTdrtflMlgVAilrb3np6+I/tmXELyShQYNOe6eI6tqCfAYnCd3YCNeQgUt56b6629sibwLN/fqQzZmdCwrOrgLKVJ0vNb01JHJJrYQGkWF/gs5ST7fPhqKPYB7+423w24VYKJOWX4sjy/u9CRIQR4Peb+8ZJV9H5DevUAkCziIxlckmEwKfAPxhu4cTWbElCcH7EjmmkKA9MC1oOTSgSSqoYxfpsPZUJNvnMMZm4srErfmcsel5hF+7PI8aWSuR+8R8wndV5hk92dP9N8gEN6eylOmV84B2NxwrTeUc5aRMO/aa8XVvRA31FJFBhGY0WCtDRSfkrJsquZY9EHBUcAtPlPn9qzxr/qVJBhdu4XsHnhyA4XxyDoLgzf1KiIS/IO8UzFPbrFal2RNzGCf/1g88BuxkYRZa2ke+GLd7zj/23big37J1UksQ30/H6I1Y/ovzD5jsU= generated-by-azure"
    username   = "adminodoo"
  }
  boot_diagnostics {
  }
  identity {
    type = "SystemAssigned"
  }
  network_interface {
    name                      = "NicVmssOdoo"
    network_security_group_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Network/networkSecurityGroups/VmssOdoo-nsg"
    primary                   = true
    ip_configuration {
      application_gateway_backend_address_pool_ids = ["/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Network/applicationGateways/AppGatewayTA/backendAddressPools/BackendOdoo"]
      name                                         = "NicVmssOdoo-defaultIpConfiguration"
      primary                                      = true
      subnet_id                                    = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Network/virtualNetworks/Vnet_TA/subnets/Backend_subnet_TA"
    }
  }
  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "StandardSSD_LRS"
  }
  source_image_reference {
    offer     = "0001-com-ubuntu-server-jammy"
    publisher = "canonical"
    sku       = "22_04-lts-gen2"
    version   = "latest"
  }
  depends_on = [
    azurerm_application_gateway.res-6,
    azurerm_network_security_group.res-8,
    azurerm_subnet.res-20,
  ]
}
resource "azurerm_virtual_machine_scale_set_extension" "res-3" {
  name                         = "AzureMonitorLinuxAgent"
  publisher                    = "Microsoft.Azure.Monitor"
  type                         = "AzureMonitorLinuxAgent"
  type_handler_version         = "1.0"
  virtual_machine_scale_set_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Compute/virtualMachineScaleSets/VmssOdoo"
  depends_on = [
    azurerm_linux_virtual_machine_scale_set.res-2,
  ]
}
resource "azurerm_virtual_machine_scale_set_extension" "res-4" {
  name      = "CustomScript"
  publisher = "Microsoft.Azure.Extensions"
  settings = jsonencode({
    commandToExecute = "./custom_app_odoo.sh"
    fileUris         = ["https://gitlab.com/yapc1/terredazure/-/raw/main/Arnaud/odoo-postgres/customapp/custom_app_odoo.sh"]
  })
  type                         = "CustomScript"
  type_handler_version         = "2.0"
  virtual_machine_scale_set_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Compute/virtualMachineScaleSets/VmssOdoo"
  depends_on = [
    azurerm_linux_virtual_machine_scale_set.res-2,
  ]
}
resource "azurerm_postgresql_flexible_server" "res-5" {
  location            = "francecentral"
  name                = "db-psql-odoo-test"
  resource_group_name = "OCC_ASD_Equipe4"
  zone                = "1"
  sku_name            = "GP_Standard_D2ds_v5"
  version             = "16"
  administrator_login         = "adminodoo"
  administrator_password      = "Azerty123"
  storage_mb             = 32768
  backup_retention_days  = 7
  high_availability {
    mode                      = "SameZone"
    standby_availability_zone = "1"
  }
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_application_gateway" "res-6" {
  enable_http2        = true
  location            = "francecentral"
  name                = "AppGatewayTA"
  resource_group_name = "OCC_ASD_Equipe4"
  zones               = ["1", "2", "3"]
  autoscale_configuration {
    max_capacity = 10
    min_capacity = 1
  }
  backend_address_pool {
    name = "BackendOdoo"
  }
  backend_http_settings {
    affinity_cookie_name  = "ApplicationGatewayAffinity"
    cookie_based_affinity = "Enabled"
    name                  = "Http"
    port                  = 80
    protocol              = "Http"
    request_timeout       = 20
  }
  frontend_ip_configuration {
    name                 = "appGwPublicFrontendIpIPv4"
    public_ip_address_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Network/publicIPAddresses/AppGateway_pub_Ip_TA"
  }
  frontend_port {
    name = "port_80"
    port = 80
  }
  gateway_ip_configuration {
    name      = "appGatewayIpConfig"
    subnet_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Network/virtualNetworks/Vnet_TA/subnets/AppGateway_TA_subnet"
  }
  http_listener {
    frontend_ip_configuration_name = "appGwPublicFrontendIpIPv4"
    frontend_port_name             = "port_80"
    name                           = "HttpListener"
    protocol                       = "Http"
  }
  request_routing_rule {
    backend_address_pool_name  = "BackendOdoo"
    backend_http_settings_name = "Http"
    http_listener_name         = "HttpListener"
    name                       = "HttpRule"
    priority                   = 100
    rule_type                  = "Basic"
  }
  sku {
    name = "WAF_v2"
    tier = "WAF_v2"
  }
  waf_configuration {
    enabled          = true
    firewall_mode    = "Prevention"
    rule_set_version = "3.0"
  }
  depends_on = [
    azurerm_public_ip.res-14,
    azurerm_subnet.res-19,
  ]
}
resource "azurerm_firewall" "res-7" {
  location            = "francecentral"
  name                = "FirewallTA"
  resource_group_name = "OCC_ASD_Equipe4"
  sku_name            = "AZFW_VNet"
  sku_tier            = "Standard"
  zones               = ["1", "2", "3"]
  ip_configuration {
    name                 = "Firewall_pub_Ip_TA"
    public_ip_address_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Network/publicIPAddresses/Firewall_pub_Ip_TA"
    subnet_id            = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Network/virtualNetworks/Vnet_TA/subnets/AzureFirewallSubnet"
  }
  depends_on = [
    azurerm_public_ip.res-15,
    azurerm_subnet.res-18,
  ]
}
resource "azurerm_network_security_group" "res-8" {
  location            = "francecentral"
  name                = "VmssOdoo-nsg"
  resource_group_name = "OCC_ASD_Equipe4"
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_network_security_rule" "res-9" {
  access                      = "Allow"
  destination_address_prefix  = "*"
  destination_port_range      = "80"
  direction                   = "Inbound"
  name                        = "AllowAnyHTTPInbound"
  network_security_group_name = "VmssOdoo-nsg"
  priority                    = 1010
  protocol                    = "Tcp"
  resource_group_name         = "OCC_ASD_Equipe4"
  source_address_prefix       = "*"
  source_port_range           = "*"
  depends_on = [
    azurerm_network_security_group.res-8,
  ]
}
resource "azurerm_network_security_rule" "res-10" {
  access                      = "Allow"
  destination_address_prefix  = "*"
  destination_port_range      = "80"
  direction                   = "Outbound"
  name                        = "AllowAnyHTTPOutbound"
  network_security_group_name = "VmssOdoo-nsg"
  priority                    = 1020
  protocol                    = "Tcp"
  resource_group_name         = "OCC_ASD_Equipe4"
  source_address_prefix       = "*"
  source_port_range           = "*"
  depends_on = [
    azurerm_network_security_group.res-8,
  ]
}
resource "azurerm_network_security_rule" "res-11" {
  access                      = "Allow"
  destination_address_prefix  = "*"
  destination_port_range      = "22"
  direction                   = "Inbound"
  name                        = "default-allow-ssh"
  network_security_group_name = "VmssOdoo-nsg"
  priority                    = 1000
  protocol                    = "Tcp"
  resource_group_name         = "OCC_ASD_Equipe4"
  source_address_prefix       = "*"
  source_port_range           = "*"
  depends_on = [
    azurerm_network_security_group.res-8,
  ]
}
resource "azurerm_private_dns_zone" "res-12" {
  name                = "privatelink.postgres.database.azure.com"
  resource_group_name = "OCC_ASD_Equipe4"
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_private_dns_zone_virtual_network_link" "res-12b" {
  name                  = "d4vp67vhjyixm"
  private_dns_zone_name = "privatelink.postgres.database.azure.com"
  resource_group_name   = "OCC_ASD_Equipe4"
  virtual_network_id    = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Network/virtualNetworks/Vnet_TA"
  depends_on = [
    azurerm_private_dns_zone.res-12,
    azurerm_virtual_network.res-16,
  ]
}
resource "azurerm_private_endpoint" "res-13" {
  location            = "francecentral"
  name                = "EndpointDbTA"
  resource_group_name = "OCC_ASD_Equipe4"
  subnet_id           = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Network/virtualNetworks/Vnet_TA/subnets/Backend_subnet_TA"
  private_dns_zone_group {
    name                 = "default"
    private_dns_zone_ids = ["/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Network/privateDnsZones/privatelink.postgres.database.azure.com"]
  }
  private_service_connection {
    is_manual_connection           = false
    name                           = "EndpointDbTA_b3e417c4-a55e-49ac-954a-79b3e69a101c"
    private_connection_resource_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-psql-odoo-test"
    subresource_names              = ["postgresqlServer"]
  }
  depends_on = [
    azurerm_postgresql_flexible_server.res-5,
    azurerm_private_dns_zone.res-12,
    azurerm_subnet.res-20,
  ]
}
resource "azurerm_public_ip" "res-14" {
  allocation_method   = "Static"
  location            = "francecentral"
  name                = "AppGateway_pub_Ip_TA"
  resource_group_name = "OCC_ASD_Equipe4"
  sku                 = "Standard"
  zones               = ["1", "2", "3"]
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_public_ip" "res-15" {
  allocation_method   = "Static"
  location            = "francecentral"
  name                = "Firewall_pub_Ip_TA"
  resource_group_name = "OCC_ASD_Equipe4"
  sku                 = "Standard"
  zones               = ["1", "2", "3"]
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_virtual_network" "res-16" {
  address_space       = ["10.1.0.0/16"]
  location            = "francecentral"
  name                = "Vnet_TA"
  resource_group_name = "OCC_ASD_Equipe4"
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_subnet" "res-17" {
  address_prefixes     = ["10.1.3.0/26"]
  name                 = "AzureBastionSubnet"
  resource_group_name  = "OCC_ASD_Equipe4"
  virtual_network_name = "Vnet_TA"
  depends_on = [
    azurerm_virtual_network.res-16,
  ]
}
resource "azurerm_subnet" "res-18" {
  address_prefixes     = ["10.1.0.0/26"]
  name                 = "AzureFirewallSubnet"
  resource_group_name  = "OCC_ASD_Equipe4"
  virtual_network_name = "Vnet_TA"
  depends_on = [
    azurerm_virtual_network.res-16,
  ]
}
resource "azurerm_subnet" "res-19" {
  address_prefixes     = ["10.1.1.0/24"]
  name                 = "AppGateway_TA_subnet"
  resource_group_name  = "OCC_ASD_Equipe4"
  virtual_network_name = "Vnet_TA"
  depends_on = [
    azurerm_virtual_network.res-16,
  ]
}
resource "azurerm_subnet" "res-20" {
  address_prefixes     = ["10.1.2.0/24"]
  name                 = "Backend_subnet_TA"
  resource_group_name  = "OCC_ASD_Equipe4"
  virtual_network_name = "Vnet_TA"
  depends_on = [
    azurerm_virtual_network.res-16,
  ]
}
resource "azurerm_monitor_action_group" "res-21" {
  name                = "cpu alert"
  resource_group_name = "OCC_ASD_Equipe4"
  short_name          = "cpu alert"
  email_receiver {
    email_address           = "nonofx31@gmail.com"
    name                    = "Email0_-EmailAction-"
    use_common_alert_schema = true
  }
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_monitor_autoscale_setting" "res-22" {
  location            = "francecentral"
  name                = "VmssOdooautoscale"
  resource_group_name = "OCC_ASD_Equipe4"
  target_resource_id  = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Compute/virtualMachineScaleSets/VmssOdoo"
  profile {
    name = "ScaleRuleOdoo"
    capacity {
      default = 1
      maximum = 8
      minimum = 1
    }
    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_namespace   = "microsoft.compute/virtualmachinescalesets"
        metric_resource_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Compute/virtualMachineScaleSets/VmssOdoo"
        operator           = "GreaterThan"
        statistic          = "Average"
        threshold          = 50
        time_aggregation   = "Average"
        time_grain         = "PT1M"
        time_window        = "PT2M"
      }
      scale_action {
        cooldown  = "PT5M"
        direction = "Increase"
        type      = "ChangeCount"
        value     = 1
      }
    }
    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_namespace   = "microsoft.compute/virtualmachinescalesets"
        metric_resource_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Compute/virtualMachineScaleSets/VmssOdoo"
        operator           = "LessThan"
        statistic          = "Average"
        threshold          = 20
        time_aggregation   = "Average"
        time_grain         = "PT1M"
        time_window        = "PT2M"
      }
      scale_action {
        cooldown  = "PT5M"
        direction = "Decrease"
        type      = "ChangeCount"
        value     = 1
      }
    }
  }
  depends_on = [
    azurerm_linux_virtual_machine_scale_set.res-2,
  ]
}
resource "azurerm_monitor_metric_alert" "res-23" {
  name                = "cpu alert"
  resource_group_name = "OCC_ASD_Equipe4"
  scopes              = ["/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/Microsoft.Compute/virtualMachineScaleSets/VmssOdoo"]
  severity            = 2
  action {
    action_group_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Equipe4/providers/microsoft.insights/actiongroups/cpu alert"
  }
  criteria {
    aggregation      = "Average"
    metric_name      = "CPU Credits Remaining"
    metric_namespace = "Microsoft.Compute/virtualMachineScaleSets"
    operator         = "GreaterThan"
    threshold        = 50
  }
  depends_on = [
    azurerm_linux_virtual_machine_scale_set.res-2,
  ]
}
resource "azurerm_private_dns_a_record" "res-24" {
  name                = "db-psql-odoo-test"
  records             = ["10.1.2.5"]
  resource_group_name = "OCC_ASD_Equipe4"
  tags = {
    creator = "created by private endpoint EndpointDbTA with resource guid 1fb90e0f-b5ae-4bda-8d97-65524fe66e2a"
  }
  ttl       = 10
  zone_name = "privatelink.postgres.database.azure.com"
  depends_on = [
    azurerm_private_dns_zone.res-12,
  ]
}
```

![alt text](pictures/08_terraform_apply.png)
