provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "example" {
  name     = "yiming_test_vm"
  location = "francecentral"
}

resource "azurerm_virtual_network" "example" {
  name                = "VNet_test"
  address_space       = ["10.0.12.0/24"]
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
}

resource "azurerm_subnet" "example" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.example.name
  virtual_network_name = azurerm_virtual_network.example.name
  address_prefixes     = ["10.0.12.0/26"]
}

resource "azurerm_public_ip" "example" {
  name                = "example-public-ip"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  allocation_method   = "Dynamic"
}

resource "azurerm_network_interface" "example" {
  name                = "example-nic"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.example.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.example.id
  }
}

resource "azurerm_linux_virtual_machine" "example" {
  name                = "example-vm"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  network_interface_ids = [azurerm_network_interface.example.id]
  size                = "Standard_DS1_v2"

  admin_username = "adminuser"

  admin_ssh_key {
    username   = "adminuser"
    public_key = file("~/.ssh/id_rsa.pub") // Chemin de la clé publique SSH locale
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  provision_vm_agent = true
}

resource "null_resource" "configure_ssh_key_permissions" {
  provisioner "local-exec" {
    command = "chmod 600 ~/.ssh/id_rsa"
  }
}

resource "null_resource" "install_odoo_sql_client" {
  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y odoo",
    ]

    connection {
      type     = "ssh"
      host     = azurerm_public_ip.example.ip_address
      user     = "adminuser"
      password = file("~/.ssh/id_rsa") // Chemin de la clé privée SSH locale
    }
  }

  depends_on = [azurerm_linux_virtual_machine.example]
}
