# Resource Group
resource "azurerm_resource_group" "rg" {
  location = "francecentral"
  name     = "OCC_ASD_Yiming_terredazure"
}

# IP publique
resource "azurerm_public_ip" "pip" {
  name                = "myAGPublicIPAddress"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  allocation_method   = "Static"
  sku                 = "Standard"
}

# Virtual Network
resource "azurerm_virtual_network" "my_terraform_network" {
  name                = "terredazure_Vnet"
  address_space       = ["10.0.12.0/24"]
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
}

# Subnet 1
resource "azurerm_subnet" "my_terraform_subnet_1" {
  name                 = "subnet-AG-frontend"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.my_terraform_network.name
  address_prefixes     = ["10.0.12.0/26"]
}

# Subnet 2
resource "azurerm_subnet" "my_terraform_subnet_2" {
  name                 = "subnet-AG-backend"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.my_terraform_network.name
  address_prefixes     = ["10.0.12.64/26"]
}

# Subnet 3
resource "azurerm_subnet" "my_terraform_subnet_3" {
  name                 = "subnet-3"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.my_terraform_network.name
  address_prefixes     = ["10.0.12.128/26"]
}

# Subnet 4
resource "azurerm_subnet" "my_terraform_subnet_4" {
  name                 = "subnet-4"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.my_terraform_network.name
  address_prefixes     = ["10.0.12.192/26"]
}