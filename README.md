# Liste des ressources avec lien

- [Azure Terraform](https://learn.microsoft.com/fr-fr/azure/developer/terraform/overview)
    - [GitLab dédié](https://gitlab.com/arnaudmarty/terraform)

- [Azure Ansible](https://learn.microsoft.com/fr-fr/azure/developer/ansible/overview)

- [Azure Jenkins](https://learn.microsoft.com/fr-fr/azure/developer/jenkins/overview)

- [Azure Application Gateway](https://learn.microsoft.com/en-us/azure/application-gateway/overview?source=recommendations)
    - avec Azure Web Application Firewall (WAF)

- [Azure Virtual Machine Scale Sets](https://learn.microsoft.com/en-us/azure/virtual-machine-scale-sets/overview)

- [Docker Odoo](https://hub.docker.com/_/odoo/) : Image Azure Ubuntu Docker custom
    - `imageID="ntegralinc1586961136942:ntg_ubuntu_22_04_docker:ntg_ubuntu_22_04_docker:1.0.0"` (Obtenir l'Urn dans le CLI Azure avec --az vm image list--)
    - `sizeVM="Standard_B2s"` (évolutif)
    - `storageSKU="StandardSSD_LRS"`

- [Serveur flexible](https://learn.microsoft.com/fr-fr/azure/postgresql/flexible-server/overview?source=recommendations) - Azure Database pour PostgreSQL (niveaux Usage général ou À mémoire optimisée)
    - haute disponibilité
    - mise à l’échelle
    - module de chiffrement conforme à la norme FIPS 140-2
    - monitoring des performances et alerte

- [Backup and restore](https://learn.microsoft.com/en-us/azure/postgresql/flexible-server/concepts-backup-restore) in Azure Database for PostgreSQL - Flexible Server

- [Stockage Blob Azure](https://learn.microsoft.com/fr-fr/azure/storage/blobs/storage-blobs-introduction)

- [Azure Key Vault](https://learn.microsoft.com/fr-fr/azure/key-vault/general/basic-concepts)
    - [GitLab dédié](https://gitlab.com/arnaudmarty/azure-vault)

- [Azure Bastion](https://learn.microsoft.com/fr-fr/azure/bastion/bastion-overview)

- [Azure DDos Protection](https://learn.microsoft.com/fr-fr/azure/ddos-protection/ddos-protection-overview)

- [Azure Monitor](https://learn.microsoft.com/fr-fr/azure/azure-monitor/overview)

- [Nom de Domain NoIp.com](https://www.noip.com)
