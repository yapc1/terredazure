resource "azurerm_resource_group" "res-0" {
  location = "francecentral"
  name     = "OCC_ASD_CapitaineAsdOcc_4"
}
resource "azurerm_ssh_public_key" "res-1" {
  location            = "francecentral"
  name                = "odoo_key"
  public_key          = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDI/LwxoZazNHrJucPEyJb9IPwai6KWwQ3wp9ACUmYxG3RT7IK/pccK5XUQTdrtflMlgVAilrb3np6+I/tmXELyShQYNOe6eI6tqCfAYnCd3YCNeQgUt56b6629sibwLN/fqQzZmdCwrOrgLKVJ0vNb01JHJJrYQGkWF/gs5ST7fPhqKPYB7+423w24VYKJOWX4sjy/u9CRIQR4Peb+8ZJV9H5DevUAkCziIxlckmEwKfAPxhu4cTWbElCcH7EjmmkKA9MC1oOTSgSSqoYxfpsPZUJNvnMMZm4srErfmcsel5hF+7PI8aWSuR+8R8wndV5hk92dP9N8gEN6eylOmV84B2NxwrTeUc5aRMO/aa8XVvRA31FJFBhGY0WCtDRSfkrJsquZY9EHBUcAtPlPn9qzxr/qVJBhdu4XsHnhyA4XxyDoLgzf1KiIS/IO8UzFPbrFal2RNzGCf/1g88BuxkYRZa2ke+GLd7zj/23big37J1UksQ30/H6I1Y/ovzD5jsU= generated-by-azure"
  resource_group_name = "OCC_ASD_CapitaineAsdOcc_4"
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_application_gateway" "res-433" {
  enable_http2        = true
  location            = "francecentral"
  name                = "AppGatewayTA"
  resource_group_name = "OCC_ASD_CapitaineAsdOcc_4"
  zones               = ["1", "2", "3"]
  autoscale_configuration {
    max_capacity = 10
    min_capacity = 1
  }
  backend_address_pool {
    name = "BackendOdoo"
  }
  backend_http_settings {
    affinity_cookie_name  = "ApplicationGatewayAffinity"
    cookie_based_affinity = "Disabled"
    name                  = "Http"
    port                  = 80
    protocol              = "Http"
    request_timeout       = 20
  }
  frontend_ip_configuration {
    name                 = "appGwPublicFrontendIpIPv4"
    public_ip_address_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Network/publicIPAddresses/AppGatewayPublicIP"
  }
  frontend_port {
    name = "port_80"
    port = 80
  }
  gateway_ip_configuration {
    name      = "appGatewayIpConfig"
    subnet_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Network/virtualNetworks/myVNet/subnets/myAppGatewaySubnet"
  }
  http_listener {
    frontend_ip_configuration_name = "appGwPublicFrontendIpIPv4"
    frontend_port_name             = "port_80"
    name                           = "HttpListener"
    protocol                       = "Http"
  }
  request_routing_rule {
    backend_address_pool_name  = "BackendOdoo"
    backend_http_settings_name = "Http"
    http_listener_name         = "HttpListener"
    name                       = "HttpRule"
    priority                   = 100
    rule_type                  = "Basic"
  }
  sku {
    name = "WAF_v2"
    tier = "WAF_v2"
  }
  waf_configuration {
    enabled          = true
    firewall_mode    = "Prevention"
    rule_set_version = "3.0"
  }
  depends_on = [
    azurerm_public_ip.res-441,
    azurerm_subnet.res-446,
  ]
}
resource "azurerm_linux_virtual_machine_scale_set" "res-2" {
  admin_username         = "adminodoo"
  instances              = 1
  location               = "francecentral"
  name                   = "VmssOdoo"
  overprovision          = false
  resource_group_name    = "OCC_ASD_CapitaineAsdOcc_4"
  secure_boot_enabled    = true
  single_placement_group = false
  sku                    = "Standard_B1s"
  upgrade_mode           = "Automatic"
  vtpm_enabled           = true
  zones                  = ["1", "2", "3"]
  admin_ssh_key {
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDI/LwxoZazNHrJucPEyJb9IPwai6KWwQ3wp9ACUmYxG3RT7IK/pccK5XUQTdrtflMlgVAilrb3np6+I/tmXELyShQYNOe6eI6tqCfAYnCd3YCNeQgUt56b6629sibwLN/fqQzZmdCwrOrgLKVJ0vNb01JHJJrYQGkWF/gs5ST7fPhqKPYB7+423w24VYKJOWX4sjy/u9CRIQR4Peb+8ZJV9H5DevUAkCziIxlckmEwKfAPxhu4cTWbElCcH7EjmmkKA9MC1oOTSgSSqoYxfpsPZUJNvnMMZm4srErfmcsel5hF+7PI8aWSuR+8R8wndV5hk92dP9N8gEN6eylOmV84B2NxwrTeUc5aRMO/aa8XVvRA31FJFBhGY0WCtDRSfkrJsquZY9EHBUcAtPlPn9qzxr/qVJBhdu4XsHnhyA4XxyDoLgzf1KiIS/IO8UzFPbrFal2RNzGCf/1g88BuxkYRZa2ke+GLd7zj/23big37J1UksQ30/H6I1Y/ovzD5jsU= generated-by-azure"
    username   = "adminodoo"
  }
  boot_diagnostics {
  }
  network_interface {
    name                      = "NicVmssOdoo"
    network_security_group_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Network/networkSecurityGroups/VmssOdoo-nsg"
    primary                   = true
    ip_configuration {
      application_gateway_backend_address_pool_ids = ["/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Network/applicationGateways/AppGatewayTA/backendAddressPools/BackendOdoo"]
      name                                         = "NicVmssOdoo-defaultIpConfiguration"
      primary                                      = true
      subnet_id                                    = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Network/virtualNetworks/myVNet/subnets/myBackendSubnet"
    }
  }
  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "StandardSSD_LRS"
  }
  source_image_reference {
    offer     = "0001-com-ubuntu-server-jammy"
    publisher = "canonical"
    sku       = "22_04-lts-gen2"
    version   = "latest"
  }
  depends_on = [
    azurerm_network_security_group.res-435,
    azurerm_subnet.res-447,
    azurerm_application_gateway.res-433,
  ]
}
resource "azurerm_postgresql_flexible_server" "res-4" {
  location            = "francecentral"
  name                = "db-odoo-tb"
  resource_group_name = "OCC_ASD_CapitaineAsdOcc_4"
  zone                = "1"
  sku_name            = "GP_Standard_D2ds_v5"
  version             = "16"
  administrator_login         = "adminodoo"
  administrator_password      = "Azerty123"
  high_availability {
    mode                      = "SameZone"
    standby_availability_zone = "1"
  }
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-5" {
  name      = "DateStyle"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "ISO, MDY"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-6" {
  name      = "IntervalStyle"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "postgres"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-7" {
  name      = "TimeZone"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "UTC"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-8" {
  name      = "allow_in_place_tablespaces"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-9" {
  name      = "allow_system_table_mods"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-12" {
  name      = "archive_command"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "BlobLogUpload.sh %f %p"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-14" {
  name      = "archive_mode"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "always"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-15" {
  name      = "archive_timeout"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "300"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-16" {
  name      = "array_nulls"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-17" {
  name      = "authentication_timeout"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "30"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-18" {
  name      = "auto_explain.log_analyze"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-19" {
  name      = "auto_explain.log_buffers"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-20" {
  name      = "auto_explain.log_format"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "text"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-21" {
  name      = "auto_explain.log_level"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "log"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-22" {
  name      = "auto_explain.log_min_duration"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "-1"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-23" {
  name      = "auto_explain.log_nested_statements"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-24" {
  name      = "auto_explain.log_settings"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-25" {
  name      = "auto_explain.log_timing"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-26" {
  name      = "auto_explain.log_triggers"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-27" {
  name      = "auto_explain.log_verbose"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-28" {
  name      = "auto_explain.log_wal"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-29" {
  name      = "auto_explain.sample_rate"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "1.0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-30" {
  name      = "autovacuum"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-31" {
  name      = "autovacuum_analyze_scale_factor"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0.1"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-32" {
  name      = "autovacuum_analyze_threshold"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "50"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-33" {
  name      = "autovacuum_freeze_max_age"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "200000000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-34" {
  name      = "autovacuum_max_workers"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "3"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-35" {
  name      = "autovacuum_multixact_freeze_max_age"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "400000000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-36" {
  name      = "autovacuum_naptime"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "60"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-37" {
  name      = "autovacuum_vacuum_cost_delay"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "2"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-38" {
  name      = "autovacuum_vacuum_cost_limit"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "-1"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-39" {
  name      = "autovacuum_vacuum_insert_scale_factor"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0.2"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-40" {
  name      = "autovacuum_vacuum_insert_threshold"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "1000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-41" {
  name      = "autovacuum_vacuum_scale_factor"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0.2"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-42" {
  name      = "autovacuum_vacuum_threshold"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "50"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-43" {
  name      = "autovacuum_work_mem"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "-1"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-44" {
  name      = "azure.accepted_password_auth_method"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "md5"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-45" {
  name      = "azure.enable_temp_tablespaces_on_local_ssd"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-47" {
  name      = "azure_storage.allow_network_access"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-48" {
  name      = "azure_storage.blob_block_size_mb"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "256"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-49" {
  name      = "azure_storage.public_account_access"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-50" {
  name      = "backend_flush_after"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "256"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-51" {
  name      = "backslash_quote"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "safe_encoding"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-53" {
  name      = "bgwriter_delay"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "20"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-54" {
  name      = "bgwriter_flush_after"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "64"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-55" {
  name      = "bgwriter_lru_maxpages"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "100"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-56" {
  name      = "bgwriter_lru_multiplier"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "2"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-57" {
  name      = "block_size"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "8192"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-58" {
  name      = "bonjour"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-60" {
  name      = "bytea_output"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "hex"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-61" {
  name      = "check_function_bodies"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-62" {
  name      = "checkpoint_completion_target"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0.9"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-63" {
  name      = "checkpoint_flush_after"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "32"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-64" {
  name      = "checkpoint_timeout"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "600"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-65" {
  name      = "checkpoint_warning"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "30"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-66" {
  name      = "client_connection_check_interval"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-67" {
  name      = "client_encoding"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "UTF8"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-68" {
  name      = "client_min_messages"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "notice"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-70" {
  name      = "commit_delay"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-71" {
  name      = "commit_siblings"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "5"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-72" {
  name      = "compute_query_id"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "auto"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-73" {
  name      = "config_file"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "/datadrive/pg/data/postgresql.conf"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-74" {
  name      = "connection_throttle.bucket_limit"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "2000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-75" {
  name      = "connection_throttle.enable"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-76" {
  name      = "connection_throttle.factor_bias"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0.8"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-77" {
  name      = "connection_throttle.hash_entries_max"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "500"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-78" {
  name      = "connection_throttle.reset_time"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "120"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-79" {
  name      = "connection_throttle.restore_factor"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "2"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-80" {
  name      = "connection_throttle.update_time"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "20"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-81" {
  name      = "constraint_exclusion"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "partition"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-82" {
  name      = "cpu_index_tuple_cost"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0.005"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-83" {
  name      = "cpu_operator_cost"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0.0025"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-84" {
  name      = "cpu_tuple_cost"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0.01"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-85" {
  name      = "cron.database_name"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "postgres"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-86" {
  name      = "cron.log_run"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-87" {
  name      = "cron.log_statement"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-88" {
  name      = "cron.max_running_jobs"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "32"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-89" {
  name      = "cursor_tuple_fraction"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0.1"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-90" {
  name      = "data_checksums"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-91" {
  name      = "data_directory"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "/datadrive/pg/data"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-92" {
  name      = "data_directory_mode"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0700"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-93" {
  name      = "data_sync_retry"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-94" {
  name      = "db_user_namespace"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-95" {
  name      = "deadlock_timeout"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "1000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-96" {
  name      = "debug_assertions"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-97" {
  name      = "debug_discard_caches"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-98" {
  name      = "debug_parallel_query"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-99" {
  name      = "debug_pretty_print"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-100" {
  name      = "debug_print_parse"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-101" {
  name      = "debug_print_plan"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-102" {
  name      = "debug_print_rewritten"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-103" {
  name      = "default_statistics_target"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "100"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-104" {
  name      = "default_table_access_method"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "heap"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-106" {
  name      = "default_text_search_config"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "pg_catalog.english"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-107" {
  name      = "default_toast_compression"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "pglz"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-108" {
  name      = "default_transaction_deferrable"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-109" {
  name      = "default_transaction_isolation"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "read committed"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-110" {
  name      = "default_transaction_read_only"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-111" {
  name      = "dynamic_library_path"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "$libdir"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-112" {
  name      = "dynamic_shared_memory_type"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "posix"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-113" {
  name      = "effective_cache_size"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "786432"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-114" {
  name      = "effective_io_concurrency"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "1"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-115" {
  name      = "enable_async_append"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-116" {
  name      = "enable_bitmapscan"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-117" {
  name      = "enable_gathermerge"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-118" {
  name      = "enable_hashagg"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-119" {
  name      = "enable_hashjoin"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-120" {
  name      = "enable_incremental_sort"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-121" {
  name      = "enable_indexonlyscan"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-122" {
  name      = "enable_indexscan"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-123" {
  name      = "enable_material"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-124" {
  name      = "enable_memoize"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-125" {
  name      = "enable_mergejoin"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-126" {
  name      = "enable_nestloop"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-127" {
  name      = "enable_parallel_append"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-128" {
  name      = "enable_parallel_hash"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-129" {
  name      = "enable_partition_pruning"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-130" {
  name      = "enable_partitionwise_aggregate"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-131" {
  name      = "enable_partitionwise_join"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-132" {
  name      = "enable_seqscan"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-133" {
  name      = "enable_sort"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-134" {
  name      = "enable_tidscan"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-135" {
  name      = "escape_string_warning"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-136" {
  name      = "event_source"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "PostgreSQL"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-137" {
  name      = "exit_on_error"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-139" {
  name      = "extra_float_digits"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "1"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-140" {
  name      = "from_collapse_limit"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "8"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-141" {
  name      = "fsync"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-142" {
  name      = "full_page_writes"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-143" {
  name      = "geqo"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-144" {
  name      = "geqo_effort"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "5"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-145" {
  name      = "geqo_generations"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-146" {
  name      = "geqo_pool_size"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-147" {
  name      = "geqo_seed"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-148" {
  name      = "geqo_selection_bias"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "2"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-149" {
  name      = "geqo_threshold"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "12"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-150" {
  name      = "gin_fuzzy_search_limit"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-151" {
  name      = "gin_pending_list_limit"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "4096"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-152" {
  name      = "hash_mem_multiplier"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "2"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-153" {
  name      = "hba_file"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "/datadrive/pg/data/pg_hba.conf"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-154" {
  name      = "hot_standby"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-155" {
  name      = "hot_standby_feedback"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-156" {
  name      = "huge_page_size"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-157" {
  name      = "huge_pages"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "try"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-158" {
  name      = "ident_file"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "/datadrive/pg/data/pg_ident.conf"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-159" {
  name      = "idle_in_transaction_session_timeout"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-160" {
  name      = "idle_session_timeout"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-161" {
  name      = "ignore_checksum_failure"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-162" {
  name      = "ignore_invalid_pages"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-163" {
  name      = "ignore_system_indexes"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-164" {
  name      = "in_hot_standby"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-165" {
  name      = "integer_datetimes"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-166" {
  name      = "intelligent_tuning"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-167" {
  name      = "intelligent_tuning.metric_targets"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "none"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-168" {
  name      = "jit"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-169" {
  name      = "jit_above_cost"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "100000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-170" {
  name      = "jit_debugging_support"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-171" {
  name      = "jit_dump_bitcode"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-172" {
  name      = "jit_expressions"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-173" {
  name      = "jit_inline_above_cost"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "500000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-174" {
  name      = "jit_optimize_above_cost"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "500000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-175" {
  name      = "jit_profiling_support"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-176" {
  name      = "jit_provider"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "llvmjit"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-177" {
  name      = "jit_tuple_deforming"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-178" {
  name      = "join_collapse_limit"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "8"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-179" {
  name      = "krb_caseins_users"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-181" {
  name      = "lc_messages"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "en_US.utf8"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-182" {
  name      = "lc_monetary"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "en_US.utf-8"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-183" {
  name      = "lc_numeric"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "en_US.utf-8"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-184" {
  name      = "lc_time"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "en_US.utf8"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-185" {
  name      = "listen_addresses"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "*"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-186" {
  name      = "lo_compat_privileges"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-188" {
  name      = "lock_timeout"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-189" {
  name      = "log_autovacuum_min_duration"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "600000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-190" {
  name      = "log_checkpoints"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-191" {
  name      = "log_connections"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-192" {
  name      = "log_destination"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "stderr"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-193" {
  name      = "log_directory"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "log"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-194" {
  name      = "log_disconnections"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-195" {
  name      = "log_duration"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-196" {
  name      = "log_error_verbosity"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "default"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-197" {
  name      = "log_executor_stats"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-198" {
  name      = "log_file_mode"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0600"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-199" {
  name      = "log_filename"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "postgresql-%Y-%m-%d_%H%M%S.log"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-200" {
  name      = "log_hostname"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-201" {
  name      = "log_line_prefix"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "%t-%c-"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-202" {
  name      = "log_lock_waits"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-203" {
  name      = "log_min_duration_sample"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "-1"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-204" {
  name      = "log_min_duration_statement"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "-1"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-205" {
  name      = "log_min_error_statement"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "error"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-206" {
  name      = "log_min_messages"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "warning"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-207" {
  name      = "log_parameter_max_length"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "-1"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-208" {
  name      = "log_parameter_max_length_on_error"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-209" {
  name      = "log_parser_stats"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-210" {
  name      = "log_planner_stats"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-211" {
  name      = "log_recovery_conflict_waits"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-212" {
  name      = "log_replication_commands"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-213" {
  name      = "log_rotation_age"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "60"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-214" {
  name      = "log_rotation_size"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "102400"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-215" {
  name      = "log_startup_progress_interval"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "10000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-216" {
  name      = "log_statement"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "none"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-217" {
  name      = "log_statement_sample_rate"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "1"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-218" {
  name      = "log_statement_stats"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-219" {
  name      = "log_temp_files"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "-1"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-220" {
  name      = "log_timezone"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "UTC"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-221" {
  name      = "log_transaction_sample_rate"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-222" {
  name      = "log_truncate_on_rotation"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-223" {
  name      = "logfiles.download_enable"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-224" {
  name      = "logfiles.retention_days"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "3"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-225" {
  name      = "logging_collector"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-226" {
  name      = "logical_decoding_work_mem"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "65536"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-227" {
  name      = "maintenance_io_concurrency"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "10"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-228" {
  name      = "maintenance_work_mem"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "216064"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-229" {
  name      = "max_connections"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "859"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-230" {
  name      = "max_files_per_process"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "1000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-231" {
  name      = "max_function_args"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "100"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-232" {
  name      = "max_identifier_length"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "63"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-233" {
  name      = "max_index_keys"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "32"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-234" {
  name      = "max_locks_per_transaction"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "64"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-235" {
  name      = "max_logical_replication_workers"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "4"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-236" {
  name      = "max_parallel_maintenance_workers"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "2"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-237" {
  name      = "max_parallel_workers"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "8"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-238" {
  name      = "max_parallel_workers_per_gather"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "2"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-239" {
  name      = "max_pred_locks_per_page"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "2"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-240" {
  name      = "max_pred_locks_per_relation"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "-2"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-241" {
  name      = "max_pred_locks_per_transaction"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "64"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-242" {
  name      = "max_prepared_transactions"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-243" {
  name      = "max_replication_slots"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "10"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-244" {
  name      = "max_slot_wal_keep_size"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "-1"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-245" {
  name      = "max_stack_depth"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "2048"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-246" {
  name      = "max_standby_archive_delay"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "30000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-247" {
  name      = "max_standby_streaming_delay"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "30000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-248" {
  name      = "max_sync_workers_per_subscription"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "2"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-249" {
  name      = "max_wal_senders"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "10"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-250" {
  name      = "max_wal_size"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "2048"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-251" {
  name      = "max_worker_processes"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "8"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-252" {
  name      = "metrics.autovacuum_diagnostics"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-253" {
  name      = "metrics.collector_database_activity"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-254" {
  name      = "metrics.pgbouncer_diagnostics"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-255" {
  name      = "min_dynamic_shared_memory"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-256" {
  name      = "min_parallel_index_scan_size"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "64"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-257" {
  name      = "min_parallel_table_scan_size"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "1024"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-258" {
  name      = "min_wal_size"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "80"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-259" {
  name      = "parallel_leader_participation"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-260" {
  name      = "parallel_setup_cost"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "1000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-261" {
  name      = "parallel_tuple_cost"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0.1"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-262" {
  name      = "password_encryption"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "md5"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-263" {
  name      = "pg_partman_bgw.analyze"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-265" {
  name      = "pg_partman_bgw.interval"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "3600"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-266" {
  name      = "pg_partman_bgw.jobmon"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-268" {
  name      = "pg_qs.interval_length_minutes"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "15"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-269" {
  name      = "pg_qs.is_enabled_fs"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-270" {
  name      = "pg_qs.max_plan_size"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "7500"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-271" {
  name      = "pg_qs.max_query_text_length"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "6000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-272" {
  name      = "pg_qs.query_capture_mode"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "none"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-273" {
  name      = "pg_qs.retention_period_in_days"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "7"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-274" {
  name      = "pg_qs.store_query_plans"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-275" {
  name      = "pg_qs.track_utility"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-276" {
  name      = "pg_stat_statements.max"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "5000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-277" {
  name      = "pg_stat_statements.save"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-278" {
  name      = "pg_stat_statements.track"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "none"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-279" {
  name      = "pg_stat_statements.track_utility"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-280" {
  name      = "pgaudit.log"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "none"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-281" {
  name      = "pgaudit.log_catalog"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-282" {
  name      = "pgaudit.log_client"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-283" {
  name      = "pgaudit.log_level"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "log"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-284" {
  name      = "pgaudit.log_parameter"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-285" {
  name      = "pgaudit.log_relation"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-286" {
  name      = "pgaudit.log_statement_once"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-288" {
  name      = "pgbouncer.enabled"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "false"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-289" {
  name      = "pglogical.batch_inserts"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-290" {
  name      = "pglogical.conflict_log_level"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "log"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-291" {
  name      = "pglogical.conflict_resolution"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "apply_remote"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-292" {
  name      = "pglogical.use_spi"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-293" {
  name      = "pgms_stats.is_enabled_fs"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-294" {
  name      = "pgms_wait_sampling.history_period"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "100"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-295" {
  name      = "pgms_wait_sampling.is_enabled_fs"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-296" {
  name      = "pgms_wait_sampling.query_capture_mode"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "none"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-297" {
  name      = "plan_cache_mode"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "auto"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-298" {
  name      = "port"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "5432"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-299" {
  name      = "post_auth_delay"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-300" {
  name      = "postgis.gdal_enabled_drivers"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "DISABLE_ALL"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-301" {
  name      = "pre_auth_delay"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-304" {
  name      = "quote_all_identifiers"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-305" {
  name      = "random_page_cost"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "2"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-307" {
  name      = "recovery_init_sync_method"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "fsync"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-308" {
  name      = "recovery_min_apply_delay"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-309" {
  name      = "recovery_prefetch"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "try"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-311" {
  name      = "recovery_target_action"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "pause"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-312" {
  name      = "recovery_target_inclusive"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-316" {
  name      = "recovery_target_timeline"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "latest"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-318" {
  name      = "recursive_worktable_factor"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "10"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-319" {
  name      = "remove_temp_files_after_crash"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-320" {
  name      = "require_secure_transport"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-321" {
  name      = "reserved_connections"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "5"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-322" {
  name      = "restart_after_crash"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-324" {
  name      = "row_security"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-325" {
  name      = "search_path"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "\"$user\", public"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-326" {
  name      = "segment_size"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "131072"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-327" {
  name      = "seq_page_cost"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "1"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-328" {
  name      = "server_encoding"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "UTF8"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-329" {
  name      = "server_version"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "16.2"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-330" {
  name      = "server_version_num"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "160002"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-332" {
  name      = "session_replication_role"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "origin"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-333" {
  name      = "shared_buffers"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "262144"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-334" {
  name      = "shared_memory_size"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "2168"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-335" {
  name      = "shared_memory_size_in_huge_pages"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "1084"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-336" {
  name      = "shared_memory_type"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "mmap"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-337" {
  name      = "shared_preload_libraries"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "pg_cron,pg_stat_statements"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-338" {
  name      = "ssl"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-339" {
  name      = "ssl_ca_file"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "/datadrive/certs/ca.pem"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-340" {
  name      = "ssl_cert_file"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "/datadrive/certs/cert.pem"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-341" {
  name      = "ssl_ciphers"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-345" {
  name      = "ssl_ecdh_curve"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "prime256v1"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-346" {
  name      = "ssl_key_file"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "/datadrive/certs/key.pem"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-347" {
  name      = "ssl_library"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "OpenSSL"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-349" {
  name      = "ssl_min_protocol_version"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "TLSv1.2"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-351" {
  name      = "ssl_passphrase_command_supports_reload"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-352" {
  name      = "ssl_prefer_server_ciphers"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-353" {
  name      = "standard_conforming_strings"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-354" {
  name      = "statement_timeout"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-355" {
  name      = "stats_fetch_consistency"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "cache"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-356" {
  name      = "superuser_reserved_connections"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "10"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-357" {
  name      = "synchronize_seqscans"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-358" {
  name      = "synchronous_commit"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-359" {
  name      = "synchronous_standby_names"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "ANY 1 ( \"azure_standby_ad8762a58614_standby\", \"db-odoo-tb-asgc\" )"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-360" {
  name      = "syslog_facility"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "local0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-361" {
  name      = "syslog_ident"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "postgres"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-362" {
  name      = "syslog_sequence_numbers"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-363" {
  name      = "syslog_split_messages"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-364" {
  name      = "tcp_keepalives_count"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "9"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-365" {
  name      = "tcp_keepalives_idle"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "120"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-366" {
  name      = "tcp_keepalives_interval"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "30"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-367" {
  name      = "tcp_user_timeout"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-368" {
  name      = "temp_buffers"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "1024"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-369" {
  name      = "temp_file_limit"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "-1"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-370" {
  name      = "temp_tablespaces"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "temptblspace"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-371" {
  name      = "timezone_abbreviations"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "Default"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-372" {
  name      = "trace_notify"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-373" {
  name      = "trace_recovery_messages"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "log"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-374" {
  name      = "trace_sort"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-375" {
  name      = "track_activities"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-376" {
  name      = "track_activity_query_size"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "1024"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-377" {
  name      = "track_commit_timestamp"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-378" {
  name      = "track_counts"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-379" {
  name      = "track_functions"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "none"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-380" {
  name      = "track_io_timing"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-381" {
  name      = "track_wal_io_timing"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-382" {
  name      = "transaction_deferrable"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-383" {
  name      = "transaction_isolation"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "read committed"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-384" {
  name      = "transaction_read_only"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-385" {
  name      = "transform_null_equals"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-386" {
  name      = "unix_socket_directories"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "/tmp"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-388" {
  name      = "unix_socket_permissions"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0777"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-389" {
  name      = "update_process_title"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-390" {
  name      = "vacuum_cost_delay"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-391" {
  name      = "vacuum_cost_limit"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "200"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-392" {
  name      = "vacuum_cost_page_dirty"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "20"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-393" {
  name      = "vacuum_cost_page_hit"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "1"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-394" {
  name      = "vacuum_cost_page_miss"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "2"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-395" {
  name      = "vacuum_failsafe_age"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "1600000000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-396" {
  name      = "vacuum_freeze_min_age"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "50000000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-397" {
  name      = "vacuum_freeze_table_age"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "150000000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-398" {
  name      = "vacuum_multixact_failsafe_age"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "1600000000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-399" {
  name      = "vacuum_multixact_freeze_min_age"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "5000000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-400" {
  name      = "vacuum_multixact_freeze_table_age"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "150000000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-401" {
  name      = "wal_block_size"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "8192"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-402" {
  name      = "wal_buffers"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "2048"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-403" {
  name      = "wal_compression"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-405" {
  name      = "wal_decode_buffer_size"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "524288"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-406" {
  name      = "wal_init_zero"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-407" {
  name      = "wal_keep_size"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "400"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-408" {
  name      = "wal_level"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "replica"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-409" {
  name      = "wal_log_hints"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-410" {
  name      = "wal_receiver_create_temp_slot"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-411" {
  name      = "wal_receiver_status_interval"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "10"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-412" {
  name      = "wal_receiver_timeout"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "60000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-413" {
  name      = "wal_recycle"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "on"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-414" {
  name      = "wal_retrieve_retry_interval"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "5000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-415" {
  name      = "wal_segment_size"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "16777216"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-416" {
  name      = "wal_sender_timeout"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "60000"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-417" {
  name      = "wal_skip_threshold"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "2048"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-418" {
  name      = "wal_sync_method"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "fdatasync"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-419" {
  name      = "wal_writer_delay"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "200"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-420" {
  name      = "wal_writer_flush_after"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "128"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-421" {
  name      = "work_mem"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "4096"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-422" {
  name      = "xmlbinary"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "base64"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-423" {
  name      = "xmloption"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "content"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_configuration" "res-424" {
  name      = "zero_damaged_pages"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  value     = "off"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_database" "res-425" {
  name      = "azure_maintenance"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_database" "res-426" {
  name      = "azure_sys"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_database" "res-427" {
  name      = "postgres"
  server_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_postgresql_flexible_server_firewall_rule" "res-428" {
  end_ip_address   = "0.0.0.0"
  name             = "AllowAllAzureServicesAndResourcesWithinAzureIps_2024-4-24_8-58-26"
  server_id        = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
  start_ip_address = "0.0.0.0"
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
  ]
}
resource "azurerm_monitor_autoscale_setting" "res-429" {
  enabled             = false
  location            = "francecentral"
  name                = "VmOdooautoscale"
  resource_group_name = "OCC_ASD_CapitaineAsdOcc_4"
  target_resource_id  = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Compute/virtualMachineScaleSets/VmOdoo"
  profile {
    name = "Condition par défaut"
    capacity {
      default = 2
      maximum = 20
      minimum = 2
    }
    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_resource_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Compute/virtualMachineScaleSets/VmOdoo"
        operator           = "GreaterThan"
        statistic          = "Average"
        threshold          = 80
        time_aggregation   = "Average"
        time_grain         = "PT1M"
        time_window        = "PT10M"
      }
      scale_action {
        cooldown  = "PT5M"
        direction = "Increase"
        type      = "ChangeCount"
        value     = 1
      }
    }
    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_resource_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Compute/virtualMachineScaleSets/VmOdoo"
        operator           = "LessThan"
        statistic          = "Average"
        threshold          = 20
        time_aggregation   = "Average"
        time_grain         = "PT1M"
        time_window        = "PT10M"
      }
      scale_action {
        cooldown  = "PT5M"
        direction = "Decrease"
        type      = "ChangeCount"
        value     = 1
      }
    }
  }
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_monitor_autoscale_setting" "res-430" {
  enabled             = false
  location            = "francecentral"
  name                = "VmsOdooautoscale"
  resource_group_name = "OCC_ASD_CapitaineAsdOcc_4"
  target_resource_id  = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Compute/virtualMachineScaleSets/VmsOdoo"
  profile {
    name = "Condition par défaut"
    capacity {
      default = 1
      maximum = 8
      minimum = 1
    }
    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_resource_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Compute/virtualMachineScaleSets/VmsOdoo"
        operator           = "GreaterThan"
        statistic          = "Average"
        threshold          = 70
        time_aggregation   = "Average"
        time_grain         = "PT1M"
        time_window        = "PT10M"
      }
      scale_action {
        cooldown  = "PT5M"
        direction = "Increase"
        type      = "ChangeCount"
        value     = 1
      }
    }
    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_resource_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Compute/virtualMachineScaleSets/VmsOdoo"
        operator           = "LessThan"
        statistic          = "Average"
        threshold          = 40
        time_aggregation   = "Average"
        time_grain         = "PT1M"
        time_window        = "PT10M"
      }
      scale_action {
        cooldown  = "PT5M"
        direction = "Decrease"
        type      = "ChangeCount"
        value     = 1
      }
    }
  }
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_monitor_autoscale_setting" "res-431" {
  enabled             = false
  location            = "francecentral"
  name                = "myScaleSettings"
  resource_group_name = "OCC_ASD_CapitaineAsdOcc_4"
  target_resource_id  = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Compute/virtualMachineScaleSets/myVMSS"
  notification {
    email {
    }
  }
  profile {
    name = "default"
    capacity {
      default = 2
      maximum = 10
      minimum = 1
    }
  }
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_monitor_autoscale_setting" "res-432" {
  enabled             = false
  location            = "francecentral"
  name                = "vmScaleOdooautoscale"
  resource_group_name = "OCC_ASD_CapitaineAsdOcc_4"
  target_resource_id  = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Compute/virtualMachineScaleSets/vmScaleOdoo"
  profile {
    name = "Condition par défaut"
    capacity {
      default = 2
      maximum = 20
      minimum = 2
    }
    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_resource_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Compute/virtualMachineScaleSets/vmScaleOdoo"
        operator           = "GreaterThan"
        statistic          = "Average"
        threshold          = 80
        time_aggregation   = "Average"
        time_grain         = "PT1M"
        time_window        = "PT10M"
      }
      scale_action {
        cooldown  = "PT5M"
        direction = "Increase"
        type      = "ChangeCount"
        value     = 1
      }
    }
    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_resource_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Compute/virtualMachineScaleSets/vmScaleOdoo"
        operator           = "LessThan"
        statistic          = "Average"
        threshold          = 20
        time_aggregation   = "Average"
        time_grain         = "PT1M"
        time_window        = "PT10M"
      }
      scale_action {
        cooldown  = "PT5M"
        direction = "Decrease"
        type      = "ChangeCount"
        value     = 1
      }
    }
  }
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_firewall" "res-434" {
  location            = "francecentral"
  name                = "FirewallTA"
  resource_group_name = "OCC_ASD_CapitaineAsdOcc_4"
  sku_name            = "AZFW_VNet"
  sku_tier            = "Standard"
  zones               = ["1", "2", "3"]
  ip_configuration {
    name                 = "FirewallPublicIp"
    public_ip_address_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Network/publicIPAddresses/FirewallPublicIp"
    subnet_id            = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Network/virtualNetworks/myVNet/subnets/AzureFirewallSubnet"
  }
  depends_on = [
    azurerm_public_ip.res-442,
    azurerm_subnet.res-445,
  ]
}
resource "azurerm_network_security_group" "res-435" {
  location            = "francecentral"
  name                = "VmssOdoo-nsg"
  resource_group_name = "OCC_ASD_CapitaineAsdOcc_4"
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_network_security_rule" "res-436" {
  access                      = "Allow"
  destination_address_prefix  = "*"
  destination_port_range      = "22"
  direction                   = "Inbound"
  name                        = "default-allow-ssh"
  network_security_group_name = "VmssOdoo-nsg"
  priority                    = 1000
  protocol                    = "Tcp"
  resource_group_name         = "OCC_ASD_CapitaineAsdOcc_4"
  source_address_prefix       = "*"
  source_port_range           = "*"
  depends_on = [
    azurerm_network_security_group.res-435,
  ]
}
resource "azurerm_private_dns_zone" "res-437" {
  name                = "privatelink.postgres.database.azure.com"
  resource_group_name = "OCC_ASD_CapitaineAsdOcc_4"
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_private_dns_zone_virtual_network_link" "res-438" {
  name                  = "d4vp67vhjyixm"
  private_dns_zone_name = "privatelink.postgres.database.azure.com"
  resource_group_name   = "OCC_ASD_CapitaineAsdOcc_4"
  virtual_network_id    = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Network/virtualNetworks/myVNet"
  depends_on = [
    azurerm_private_dns_zone.res-437,
    azurerm_virtual_network.res-443,
  ]
}
resource "azurerm_private_endpoint" "res-439" {
  location            = "francecentral"
  name                = "EndpointDbTA"
  resource_group_name = "OCC_ASD_CapitaineAsdOcc_4"
  subnet_id           = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Network/virtualNetworks/myVNet/subnets/myBackendSubnet"
  private_dns_zone_group {
    name                 = "default"
    private_dns_zone_ids = ["/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Network/privateDnsZones/privatelink.postgres.database.azure.com"]
  }
  private_service_connection {
    is_manual_connection           = false
    name                           = "EndpointDbTA_b3e417c4-a55e-49ac-954a-79b3e69a101c"
    private_connection_resource_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.DBforPostgreSQL/flexibleServers/db-odoo-tb"
    subresource_names              = ["postgresqlServer"]
  }
  depends_on = [
    azurerm_postgresql_flexible_server.res-4,
    azurerm_private_dns_zone.res-437,
    azurerm_subnet.res-447,
  ]
}
resource "azurerm_public_ip" "res-441" {
  allocation_method   = "Static"
  domain_name_label   = "occ-asd-ep04"
  location            = "francecentral"
  name                = "AppGatewayPublicIP"
  resource_group_name = "OCC_ASD_CapitaineAsdOcc_4"
  sku                 = "Standard"
  zones               = ["1", "2", "3"]
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_public_ip" "res-442" {
  allocation_method   = "Static"
  location            = "francecentral"
  name                = "FirewallPublicIp"
  resource_group_name = "OCC_ASD_CapitaineAsdOcc_4"
  sku                 = "Standard"
  zones               = ["1", "2", "3"]
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_virtual_network" "res-443" {
  address_space       = ["10.1.0.0/16"]
  location            = "francecentral"
  name                = "myVNet"
  resource_group_name = "OCC_ASD_CapitaineAsdOcc_4"
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_subnet" "res-444" {
  address_prefixes     = ["10.1.3.0/26"]
  name                 = "AzureBastionSubnet"
  resource_group_name  = "OCC_ASD_CapitaineAsdOcc_4"
  virtual_network_name = "myVNet"
  depends_on = [
    azurerm_virtual_network.res-443,
  ]
}
resource "azurerm_subnet" "res-445" {
  address_prefixes     = ["10.1.0.0/26"]
  name                 = "AzureFirewallSubnet"
  resource_group_name  = "OCC_ASD_CapitaineAsdOcc_4"
  virtual_network_name = "myVNet"
  depends_on = [
    azurerm_virtual_network.res-443,
  ]
}
resource "azurerm_subnet" "res-446" {
  address_prefixes     = ["10.1.1.0/24"]
  name                 = "myAppGatewaySubnet"
  resource_group_name  = "OCC_ASD_CapitaineAsdOcc_4"
  virtual_network_name = "myVNet"
  depends_on = [
    azurerm_virtual_network.res-443,
  ]
}
resource "azurerm_subnet" "res-447" {
  address_prefixes     = ["10.1.2.0/24"]
  name                 = "myBackendSubnet"
  resource_group_name  = "OCC_ASD_CapitaineAsdOcc_4"
  virtual_network_name = "myVNet"
  depends_on = [
    azurerm_virtual_network.res-443,
  ]
}
resource "azurerm_monitor_autoscale_setting" "res-448" {
  location            = "francecentral"
  name                = "VmssOdooautoscale"
  resource_group_name = "OCC_ASD_CapitaineAsdOcc_4"
  target_resource_id  = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Compute/virtualMachineScaleSets/VmssOdoo"
  profile {
    name = "ScaleRuleOdoo"
    capacity {
      default = 1
      maximum = 8
      minimum = 1
    }
    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_resource_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Compute/virtualMachineScaleSets/VmssOdoo"
        operator           = "GreaterThan"
        statistic          = "Average"
        threshold          = 70
        time_aggregation   = "Average"
        time_grain         = "PT1M"
        time_window        = "PT10M"
      }
      scale_action {
        cooldown  = "PT5M"
        direction = "Increase"
        type      = "ChangeCount"
        value     = 1
      }
    }
    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_resource_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_CapitaineAsdOcc_4/providers/Microsoft.Compute/virtualMachineScaleSets/VmssOdoo"
        operator           = "LessThan"
        statistic          = "Average"
        threshold          = 30
        time_aggregation   = "Average"
        time_grain         = "PT1M"
        time_window        = "PT10M"
      }
      scale_action {
        cooldown  = "PT5M"
        direction = "Decrease"
        type      = "ChangeCount"
        value     = 1
      }
    }
  }
  depends_on = [
    azurerm_linux_virtual_machine_scale_set.res-2,
  ]
}
resource "azurerm_private_dns_a_record" "res-449" {
  name                = "db-odoo-tb"
  records             = ["10.1.2.5"]
  resource_group_name = "OCC_ASD_CapitaineAsdOcc_4"
  tags = {
    creator = "created by private endpoint EndpointDbTA with resource guid 59e56d08-d5c7-4f76-97f5-4ca4d39dac01"
  }
  ttl       = 10
  zone_name = "privatelink.postgres.database.azure.com"
  depends_on = [
    azurerm_private_dns_zone.res-437,
  ]
}
