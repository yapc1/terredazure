#!/bin/bash
mkdir terra
wget https://releases.hashicorp.com/terraform/1.8.1/terraform_1.8.1_linux_amd64.zip -P $HOME/terra
mkdir $HOME/bin
unzip $HOME/terra/terraform_1.8.1_linux_amd64 -d $HOME/bin