#!/bin/bash

read -p "Enter admin password for VMs: " mdpadmin

rg_name="OCC_ASD_CapitaineAsdOcc_1"
az config set extension.use_dynamic_install=yes_without_prompt

# Create the resource group
az group create --name $rg_name --location francecentral

# Create the virtual network
az network vnet create --resource-group $rg_name --location francecentral --name myVNet --address-prefixes 10.1.0.0/16

# Create the application gateway subnet
az network vnet subnet create --resource-group $rg_name --vnet-name myVNet --name myAppGatewaySubnet --address-prefixes 10.1.1.0/24

# Create the backend subnet for VM scale set
az network vnet subnet create --resource-group $rg_name --vnet-name myVNet --name myBackendSubnet --address-prefixes 10.1.2.0/24

# Create the public IP address
az network public-ip create --resource-group $rg_name --name myPublicIPStandard --sku Standard

# Create the application gateway
az network application-gateway create --name myAppGateway --resource-group $rg_name --public-ip-address myPublicIPStandard --vnet-name myVNet --subnet myAppGatewaySubnet --capacity 2 --sku Standard_v2 --http-settings-cookie-based-affinity Enabled --frontend-port 80 --http-settings-port 80 --routing-rule-type Basic --priority 200

# Create the VM scale set
az vmss create --resource-group $rg_name --name myVMSS --image Ubuntu2204 --admin-username azureuser --admin-password $mdpadmin --instance-count 1 --upgrade-policy-mode automatic --vnet-name myVNet --subnet myBackendSubnet

# Update application gateway backend pool
az network application-gateway address-pool update --gateway-name myAppGateway --resource-group $rg_name --name appGatewayBackendPool --servers myVMSS

# Enable autoscaling for VM scale set
az monitor autoscale create --resource-group $rg_name --resource myVMSS --resource-type Microsoft.Compute/virtualMachineScaleSets --name myScaleSettings --min-count 1 --max-count 10 --count 2
